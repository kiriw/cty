import $ from 'jquery'


window.$ = window.jQuery = $

jQuery.fn.size = function() {
	return this.length;
};

// window.$ = $

import './utils/polyfills'
import 'what-input'
import './carousels'
import WOW from 'wow.js'
require ('jquery-nice-select')
// import App from './App.vue'
new WOW().init();

$('select').niceSelect()
// const app = new Vue({
// 	store: createStore(),
// 	components: {
// 		App
// 	},
// 	render: h => h(App)
// }).$mount('#app')

