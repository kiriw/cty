import Promise from 'promise-polyfill';
import 'whatwg-fetch';

!window.Promise && (window.Promise = Promise);
