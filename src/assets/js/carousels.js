import $ from 'jquery'
import 'slick-carousel'

const $responses = $('.responses')
const $advantages = $('.advantages')
const $payments = $('.payment-list')


$payments.slick({
	mobileFirst: true,
	infinite: true,
	slidesToShow: 4,
	slidesToScroll: 1,
	arrows: false,
	dots: true,
	responsive: [
	{
		breakpoint: 768,
		settings:'unslick'
	}
	]
})

$advantages.slick({
	mobileFirst: true,
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	dots: true,
	responsive: [
	{
		breakpoint: 768,
		settings:'unslick'
	}
	]
})

$responses.slick({
	centerMode: true,
	centerPadding: 0,
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: true,
	prevArrow: '<div class="click-arrow slick-prev"></div>',
	nextArrow: '<div class="click-arrow slick-next"></div>',
	mobileFirst: true,
	infinite: true,
	responsive: [
	{
		breakpoint: 768,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 1
		}
	}
	]
})

$responses.on('beforeChange', function() {
	$('.slick-arrow', $responses).hide();
})

$responses.on('afterChange', function() {
	$('.slick-arrow', $responses).fadeIn();
})